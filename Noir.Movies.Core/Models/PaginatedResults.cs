﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Noir.Movies.Core.Models
{
    public class PaginatedResults<T> where T : class
    {
        public int TotalItems { get; private set; }
        public IEnumerable<T> Results { get; private set; }
        public PagingInfo PageInfo { get; private set; }

        public PaginatedResults(IEnumerable<T> collection, PagingInfo pageInfo, int totaItems)
        {
            Results = collection;
            PageInfo = pageInfo;
            TotalItems = totaItems;
        }
    }
}
