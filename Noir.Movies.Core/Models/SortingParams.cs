﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Noir.Movies.Core.Models
{
    public class SortingParams
    {
        public string SortBy { get; set; }
    }
}
