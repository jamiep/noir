﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Noir.Movies.Core.Models
{
    public class PagingInfo
    {
        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; } = 10;

        public PagingInfo()
        {

        }
        
        public int GetSkip()
        {
            return (PageNumber - 1) * PageSize;
        }
    }
}
