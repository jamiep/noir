﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Noir.Movies.Core.Models
{
    public class Entity
    {
        [Key]
        public int Id { get; set; }
        
        public override bool Equals(object obj)
        {
            var entity = obj as Entity;
            
            if (ReferenceEquals(entity, null))
                return false;

            return entity != null && 
                this.Id == entity.Id && 
                this.GetType().Equals(obj.GetType());
        }
        
        public override int GetHashCode()
        {
            return (Id.GetHashCode() * 397) ^ this.GetType().GetHashCode();
        }
        
        public static bool operator ==(Entity a, Entity b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
                return true;
            
            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                return false;
            
            return a.Equals(b);
        }
        
        public static bool operator !=(Entity a, Entity b)
        { 
            return !(a == b);
        }
    }
}
