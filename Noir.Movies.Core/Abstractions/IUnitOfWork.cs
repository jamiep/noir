﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Noir.Movies.Core.Abstractions
{
    public interface IUnitOfWork
    {
        void Commit();
        Task CommitAsync();
    }
}
