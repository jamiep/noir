﻿using AutoMapper;
using Noir.Movies.API.Models;
using Noir.Movies.DAL.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noir.Movies.API
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<DAL.Models.Movie, ViewMovieDto>().AfterMap(MapMovieToDto);
            CreateMap<NewRatingDto, DAL.Models.MovieRating>();
            CreateMap<MovieCriteriaFiltersDto, MovieCriteriaFilters>();
        }

        public void MapMovieToDto(DAL.Models.Movie movie, ViewMovieDto dto)
        {
            // Calculates average movie rating

            if (movie.MovieRatings.Any())
            {
                var averageRating = movie.MovieRatings.Average(rating => rating.Rating);
                dto.AverageRating = Math.Round(averageRating * 2, MidpointRounding.AwayFromZero) / 2;
            }
            
            // Formats the film running time
            TimeSpan t = TimeSpan.FromSeconds(movie.RunningTime);

            dto.RunningTime = string.Format("{0:D2}h:{1:D2}m:{2:D2}s",
                            t.Hours,
                            t.Minutes,
                            t.Seconds);
        }
    }
}
