﻿using Noir.Movies.Core.Models;
using Noir.Movies.DAL.Filters;
using Noir.Movies.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noir.Movies.API.Abstractions
{
    public interface IMovieControllerRepository
    {
        Task<PaginatedResults<Movie>> FindMoviesAsync(MovieCriteriaFilters filters, PagingInfo pageInfo, SortingParams sortingOptions);
        Task<PaginatedResults<Movie>> FindTopRatedMoviesAsync(MovieRatingFilters filters, PagingInfo pageInfo);
        Task CreateOrUpdateRatingAsync(MovieRating newRating);
        Task<bool> UserExistsAsync(int userId);
        Task<bool> MovieExistsAsync(int movieId);
    }
}
