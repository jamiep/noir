﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Noir.Movies.DAL;
using Noir.Movies.DAL.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noir.Movies.API.StaticUtils
{
    public static class InitialiseDbContextManager
    {
        public static IApplicationBuilder InitialiseMovieRatingDbContext(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                serviceScope.ServiceProvider
                    .GetService<MovieRatingContext>()
                    .EnsureCreated()
                    .EnsureSeeded();
            }

            return app;
        }
    }
}
