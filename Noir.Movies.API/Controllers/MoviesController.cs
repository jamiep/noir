﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Noir.Movies.API.Abstractions;
using Noir.Movies.API.Models;
using Noir.Movies.Core.Abstractions;
using Noir.Movies.Core.Models;
using Noir.Movies.DAL.Filters;

namespace Noir.Movies.API.Controllers
{
    public class MoviesController : Controller
    {
        private readonly IMovieControllerRepository _repository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public MoviesController(IMovieControllerRepository repository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        
        [HttpGet("api/movies/")]
        public async Task<IActionResult> GetMovies([FromQuery] MovieCriteriaFiltersDto filtersDto, [FromQuery] PagingInfo pagingInfo, [FromQuery] SortingParams sortingParams)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var filters = _mapper.Map<MovieCriteriaFilters>(filtersDto);

            var dbMovies = await _repository.FindMoviesAsync(filters, pagingInfo, sortingParams);

            if(dbMovies.TotalItems == 0)
            {
                return NotFound("Movies could not be found matching search criteria.");
            }

            var movieCollection = _mapper.Map<IEnumerable<ViewMovieDto>>(dbMovies.Results);

            var movieResults = new PaginatedResults<ViewMovieDto>(movieCollection, dbMovies.PageInfo, dbMovies.TotalItems);

            // I would usually return the movieResults object but then the response wouldn't match spec.
            return Ok(movieResults.Results);
        }

        [HttpGet("api/movies/ratings")]
        public async Task<IActionResult> Get([FromQuery] MovieRatingFilters filters)
        {
            var pagingInfo = new PagingInfo();

            pagingInfo.PageSize = 5;

            var topRatedMovies = await _repository.FindTopRatedMoviesAsync(filters, pagingInfo);
            
            if (topRatedMovies.TotalItems == 0)
            {
                return NotFound("Movies could not be found matching search criteria.");
            }

            var movieCollection = _mapper.Map<IEnumerable<ViewMovieDto>>(topRatedMovies.Results);

            var movieResults = new PaginatedResults<ViewMovieDto>(movieCollection, topRatedMovies.PageInfo, topRatedMovies.TotalItems);

            // I would usually return the movieResults object but then the response wouldn't match spec.
            return Ok(movieResults.Results);
        }

        [HttpPut("api/movies/ratings")]
        public async Task<IActionResult> Rate([FromBody] NewRatingDto ratingModelDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            bool movieExists = await _repository.MovieExistsAsync(ratingModelDto.MovieId.Value);

            if (!movieExists)
            {
                return NotFound("A movie could not be fond with the specified id");
            }

            var userExists = await _repository.UserExistsAsync(ratingModelDto.UserId.Value);

            if (!userExists)
            {
                return NotFound("A user could not be fond with the specified id");
            }
            
            var movieRating = _mapper.Map<DAL.Models.MovieRating>(ratingModelDto);

            await _repository.CreateOrUpdateRatingAsync(movieRating);

            await _unitOfWork.CommitAsync();

            return Ok();
        }
    }

}
