﻿using Noir.Movies.DAL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noir.Movies.API.Models
{
    public class ViewMovieDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int YearOfRelease { get; set; }
        public MovieGenre Genre { get; set; }
        public string RunningTime { get; set; }
        public double? AverageRating { get; set; }
    }
}
