﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Noir.Movies.API.Models
{
    public class NewRatingDto
    {
        [Required]
        public int? MovieId { get; set; }
        [Required]
        public int? UserId { get; set; }

        [Range(1, 5)]
        public int Rating { get; set; }
    }
}
