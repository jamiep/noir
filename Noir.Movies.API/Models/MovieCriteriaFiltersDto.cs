﻿using Noir.Movies.API.Validators;
using Noir.Movies.DAL.Enums;
using Noir.Movies.DAL.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noir.Movies.API.Models
{
    [AtLeastOneProperty("Title", "YearOfRelease", "Genre", ErrorMessage = "At least one search criteria property must be specified")]
    public class MovieCriteriaFiltersDto
    {
        public string Title { get; set; }
        public int? YearOfRelease { get; set; }
        public MovieGenre? Genre { get; set; }

        public MovieCriteriaFiltersDto()
        {

        }
    }
}
