﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Noir.Movies.API.Abstractions;
using Noir.Movies.Core.Abstractions;
using Noir.Movies.DAL;
using Noir.Movies.DAL.Classes;
using Noir.Movies.API.StaticUtils;

namespace Noir.Movies.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Auto Mapper Configurations
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
            
            // SQL database
            services.AddDbContext<MovieRatingContext>(opt => opt.UseSqlServer(Configuration.GetConnectionString("MovieRatingsDatabaseConnectionString")));
            
            services.AddScoped<IMovieControllerRepository, Repository>();
            services.AddScoped<IUnitOfWork, Repository>();
            services.AddMvc();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
            app.InitialiseMovieRatingDbContext();
        }
    }
}
