﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Noir.Movies.API.Abstractions;
using Noir.Movies.DAL;

namespace Noir.Movies.API
{
    // New repository per layer inheriting from layer specific abstractions ( Dependancy Inversion )
    public class Repository : DAL.Repository, IMovieControllerRepository
    {
        public Repository(MovieRatingContext context) : base(context)
        {

        }
    }
}
