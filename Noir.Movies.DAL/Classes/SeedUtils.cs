﻿using Newtonsoft.Json;
using Noir.Movies.DAL.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Noir.Movies.DAL.Classes
{
    public static class SeedUtils
    {
        private static IEnumerable<T> GetData<T>() where T : class
        {
            return JsonConvert.DeserializeObject<List<T>>(File.ReadAllText(Path.Combine("SeedData", $"{ typeof(T).Name }s.json")));
        }

        public static MovieRatingContext EnsureSeeded(this MovieRatingContext context)
        {
            using (var transaction = context.Database.BeginTransaction())
            {

                context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[Movies] ON");
                if (!context.Movies.Any())
                {
                    var movies = GetData<Movie>();
                    context.Movies.AddRange(movies);
                    context.SaveChanges();
                }
                context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[Movies] OFF");

                context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[Users] ON");
                if (!context.Users.Any())
                {
                    var users = GetData<User>();
                    context.Users.AddRange(users);
                    context.SaveChanges();
                }
                context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[Users] OFF");
                
                if (!context.MovieRatings.Any())
                {
                    var movieRatings = GetData<MovieRating>();
                    context.MovieRatings.AddRange(movieRatings);
                    context.SaveChanges();
                }


                transaction.Commit();
            }

            return context;
        }
    }
}
