﻿using Microsoft.EntityFrameworkCore;
using Noir.Movies.Core.Abstractions;
using Noir.Movies.Core.Models;
using Noir.Movies.DAL.Filters;
using Noir.Movies.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Noir.Movies.DAL
{
    public class Repository : IUnitOfWork
    {
        private readonly MovieRatingContext _context;

        public Repository(MovieRatingContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Checks whether associated user record exists in database
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns></returns>
        public async Task<bool> UserExistsAsync(int userId)
        {
            return await _context.Users.AnyAsync(user => user.Id == userId);
        }

        /// <summary>
        /// Checks whether associated movie record exists in database
        /// </summary>
        /// <param name="movieId">Movie Id</param>
        /// <returns></returns>
        public async Task<bool> MovieExistsAsync(int movieId)
        {
            return await _context.Movies.AnyAsync(movie => movie.Id == movieId);
        }

        /// <summary>
        /// Find moves ordered by rating
        /// </summary>
        /// <param name="filters">Filters</param>
        /// <param name="pageInfo">Paging data</param>
        /// <returns>Paginated collection of movies</returns>
        public async Task<PaginatedResults<Movie>> FindTopRatedMoviesAsync(MovieRatingFilters filters, PagingInfo pageInfo)
        {
            var movies = _context.Movies
                .Include(movie => movie.MovieRatings);

            IOrderedQueryable<Movie> orderedMovies = null;

            if (filters.UserId.HasValue)
            {
                orderedMovies = movies
                                .Where(movie => movie.MovieRatings.Any(x => x.UserId == filters.UserId))
                                .OrderByDescending(movie => movie.MovieRatings
                                .Where(rating => rating.UserId == filters.UserId.Value)
                                .Average(p => p.Rating)).
                                ThenBy(movie => movie.Title);
            }
            else
            {
                orderedMovies = movies
                                .Where(movie => movie.MovieRatings.Any())
                                .OrderByDescending(movie => movie.MovieRatings.Average(p => p.Rating))
                                .ThenBy(movie => movie.Title);
            }
            
            var paginatedResults = await orderedMovies
                .Skip(pageInfo.GetSkip())
                .Take(pageInfo.PageSize)
                .ToListAsync();

            var total = orderedMovies.Count();

            return new PaginatedResults<Movie>(paginatedResults, pageInfo, total);
        }

        /// <summary>
        /// Find movies filtered by search criteria 
        /// </summary>
        /// <param name="filters">Filters</param>
        /// <param name="pageInfo">Paging data</param>
        /// <param name="sortingParams">Sorting parameters</param>
        /// <returns>Paginated collection of movies</returns>
        public async Task<PaginatedResults<Movie>> FindMoviesAsync(MovieCriteriaFilters filters, PagingInfo pageInfo, SortingParams sortingParams)
        {
            IQueryable<Movie> movies = _context.Movies.Include(movie => movie.MovieRatings);

            if (!string.IsNullOrEmpty(filters.Title))
            {
                movies = movies
                    .Where(movie => movie.Title.Contains(filters.Title));
            }

            if (filters.YearOfRelease.HasValue)
            {
                movies = movies
                    .Where(movie => movie.YearOfRelease == filters.YearOfRelease.Value);
            }

            if (filters.Genre.HasValue)
            {
                movies = movies
                    .Where(movie => movie.Genre == filters.Genre);
            }

            if (!string.IsNullOrEmpty(sortingParams.SortBy))
            {
                movies = movies.Sort(sortingParams.SortBy);
            }

            var paginatedResults = await movies
                .Skip(pageInfo.GetSkip())
                .Take(pageInfo.PageSize)
                .ToListAsync();

            var total = movies.Count();

            return new PaginatedResults<Movie>(paginatedResults, pageInfo, total);
        }

        /// <summary>
        /// Creates or updates a movie rating
        /// </summary>
        /// <param name="newRating">New rating db model</param>
        /// <returns></returns>
        public async Task CreateOrUpdateRatingAsync(MovieRating newRating)
        {
            var rating = _context.MovieRatings
                .FirstOrDefault(existingRating => existingRating.UserId == newRating.UserId && 
                    existingRating.MovieId == newRating.MovieId);

            if(rating != null)
            {
                rating.Rating = newRating.Rating;
                rating.LastUpdated = DateTime.Now;
                _context.Entry(rating).State = EntityState.Modified;
            }
            else
            {
                await _context.MovieRatings.AddAsync(newRating);
            }
        }

        /// <summary>
        /// Commits changes to database
        /// </summary>
        public void Commit()
        {
            _context.SaveChanges();
        }

        /// <summary>
        /// Asyncronously Commits changes to database
        /// </summary>
        /// <returns></returns>
        public async Task CommitAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
