﻿using Microsoft.EntityFrameworkCore;
using System;

namespace Noir.Movies.DAL
{
    public class MovieRatingContext : DbContext
    {
        public MovieRatingContext(DbContextOptions<MovieRatingContext> options)
            : base(options)
        {

        }
        
        public MovieRatingContext EnsureCreated()
        {
            Database.EnsureCreated();
            return this;
        }

        public DbSet<Models.Movie> Movies { get; set; }
        public DbSet<Models.MovieRating> MovieRatings { get; set; }
        public DbSet<Models.User> Users { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Models.Movie>().HasMany<Models.MovieRating>(movie => movie.MovieRatings).WithOne(x => x.Movie).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Models.User>().HasMany<Models.MovieRating>(movie => movie.MovieRatings).WithOne(x => x.User).OnDelete(DeleteBehavior.Restrict);
            
            modelBuilder.Entity<Models.MovieRating>().HasIndex(x => x.MovieId).IsUnique(false);
            modelBuilder.Entity<Models.MovieRating>().HasIndex(x => x.UserId).IsUnique(false);
        }

    }
}
