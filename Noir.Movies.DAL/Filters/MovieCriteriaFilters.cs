﻿using Noir.Movies.DAL.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Noir.Movies.DAL.Filters
{
    public class MovieCriteriaFilters
    {
        public string Title { get; set; }
        public int? YearOfRelease { get; set; }
        public MovieGenre? Genre { get; set; }

        public MovieCriteriaFilters()
        {

        }
    }
}
