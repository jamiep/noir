﻿using Noir.Movies.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Noir.Movies.DAL.Models
{
    public class MovieRating : Entity
    {
        [Required]
        [ForeignKey("User")]
        public int UserId { get; set; }
        [Required]
        [ForeignKey("Movie")]
        public int MovieId { get; set; }
        [Required]
        [Range(1, 5)]
        public int Rating { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? LastUpdated { get; set; }

        [ForeignKey("MovieId")]
        public virtual Movie Movie { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        public MovieRating()
        {
            DateCreated = DateTime.Now;
        }
    }
}
