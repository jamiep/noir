﻿using Noir.Movies.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Noir.Movies.DAL.Models
{
    public class User : Entity
    {
        public string FullName { get; set; }
        public DateTime DateCreated { get; set; } 

        public virtual IEnumerable<MovieRating> MovieRatings { get; set; }

        public User()
        {
            DateCreated = DateTime.Now;
            MovieRatings = new List<MovieRating>();
        }
    }
}
