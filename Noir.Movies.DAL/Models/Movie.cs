﻿using Noir.Movies.Core.Models;
using Noir.Movies.DAL.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Noir.Movies.DAL.Models
{
    public class Movie : Entity
    {
        public string Title { get; set; }
        public int YearOfRelease { get; set; }
        public int RunningTime { get; set; }
        public MovieGenre Genre { get; set; }
        public DateTime DateCreated { get; set; }

        public virtual IEnumerable<MovieRating> MovieRatings { get; set; }

        public Movie()
        {
            DateCreated = DateTime.Now;
            MovieRatings = new List<MovieRating>();
        }
    }
}
